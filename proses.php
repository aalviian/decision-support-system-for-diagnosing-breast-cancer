<?php
	$con = mysqli_connect('localhost', 'root', '','sispak') or die(mysqli_error());
	session_start();
	$var = 0;
	if (isset($_POST['submit'])) {
		$query = mysqli_query($con, "SELECT * FROM dataset");
		$dataGet  = array($_POST['a1'],$_POST['a2'],$_POST['a3'],$_POST['a4'],$_POST['a5'],$_POST['a6'],$_POST['a7'],$_POST['a8'],$_POST['a9']);
		$dataBaru = array();
		for ($i=0; $i < 9; $i++) { 
			$dataBaru[$i] = substr($dataGet[$i], 2);
		}
		$proses = array();
		while ($row = mysqli_fetch_array($query)) {
			$temp = pow($row['Clump_Thickness'] - $dataBaru[0],2) + 
					pow($row['Uniformity_of_Cell_Size'] - $dataBaru[1],2) + 
					pow($row['Uniformity_of_Cell_Shape'] - $dataBaru[2],2) + 
					pow($row['Marginal_Adhesion'] - $dataBaru[3],2) + 
					pow($row['Single_Epithelial_Cell_Size'] - $dataBaru[4],2) + 
					pow($row['Bare_Nuclei'] - $dataBaru[5],2) + 
					pow($row['Bland_Chromatin'] - $dataBaru[6],2) + 
					pow($row['Normal_Nucleoli'] - $dataBaru[7],2) + 
					pow($row['Mitoses'] - $dataBaru[8],2);
			
			$proses[$var] = array('result' => $temp, 'kelas' => $row['Class']);

		 	$var = $var + 1; 
		}
		$res = array();
		foreach ($proses as $row) {
			$res[] = $row['result'];
		}
		array_multisort($res, SORT_ASC, $proses);
		$malignan = 0;
		$benign = 0;
		for ($i=0; $i < 5; $i++) { 
			if ($proses[$i]['kelas']==2) {
				$benign = $benign + 1;
			}
			if ($proses[$i]['kelas']==4) {
				$malignan = $malignan + 1;
			}
		}
	
		$kelas = 0;
		if ($malignan>$benign) {
			$kelas = 4;
		}
		else{
			$kelas =2;
		}
		mysqli_query($con, "INSERT INTO dataset VALUES('','','$dataBaru[0]','$dataBaru[1]','$dataBaru[2]','$dataBaru[3]','$dataBaru[4]','$dataBaru[5]','$dataBaru[6]','$dataBaru[7]','$dataBaru[8]','$kelas')");
		$_SESSION['benign']=$benign;
		$_SESSION['malignan']=$malignan;
		$_SESSION['a1']=$dataBaru[0];
		$_SESSION['a2']=$dataBaru[1];
		$_SESSION['a3']=$dataBaru[2];
		$_SESSION['a4']=$dataBaru[3];
		$_SESSION['a5']=$dataBaru[4];
		$_SESSION['a6']=$dataBaru[5];
		$_SESSION['a7']=$dataBaru[6];
		$_SESSION['a8']=$dataBaru[7];
		$_SESSION['a9']=$dataBaru[8];

	$result = mysqli_query($con, "SELECT * FROM dataset WHERE Class = '$kelas'");
	$att1 = '';
	$att2 = '';
	$att3 = '';
	$att4 = '';
	$att5 = '';
	$att6 = '';
	$att7 = '';
	$att8 = '';
	$att9 = '';
	while ($row = mysqli_fetch_array($result)) {
		$att1 = $att1.", ".$row['Clump_Thickness'];
		$att2 = $att2.", ".$row['Uniformity_of_Cell_Size'];
		$att3 = $att3.", ".$row['Uniformity_of_Cell_Shape'];
		$att4 = $att4.", ".$row['Marginal_Adhesion'];
		$att5 = $att5.", ".$row['Single_Epithelial_Cell_Size'];
		$att6 = $att6.", ".$row['Bare_Nuclei'];
		$att7 = $att7.", ".$row['Bland_Chromatin'];
		$att8 = $att8.", ".$row['Normal_Nucleoli'];
		$att9 = $att9.", ".$row['Mitoses'];
	}

	$_SESSION['att1'] = substr($att1, 2);
	$_SESSION['att2'] = substr($att2, 2);
	$_SESSION['att3'] = substr($att3, 2);
	$_SESSION['att4'] = substr($att4, 2);
	$_SESSION['att5'] = substr($att5, 2);
	$_SESSION['att6'] = substr($att6, 2);
	$_SESSION['att7'] = substr($att7, 2);
	$_SESSION['att8'] = substr($att8, 2);
	$_SESSION['att9'] = substr($att9, 2);

}
	header('location:result.php');
	
?>