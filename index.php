<?php require 'header.php' ?>
	<div class="splash fade-in">
		<h1 class="splash-title fade-in">Breast Cancer Decision Support System</h1>
        
		<a href="diagnosis.php" class="splash-arrow fade-in"><img src="splash/img/down-arrow.png" alt=""/></a>
	</div>
<?php require 'footer.php' ?>