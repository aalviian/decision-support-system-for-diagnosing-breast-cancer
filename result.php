<?php session_start(); 
require 'header.php'; ?>
<div class="x_title">
    <h2>Diagnosis Result</h2>
    <div class="clearfix"></div>
</div>
<?php if (isset($_SESSION['benign'])) { ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
</button>
<strong>Diagnosis resulted!</strong>
</div>
<input type="hidden" id="malignan" value="<?php echo $_SESSION['malignan'];?>">
<input type="hidden" id="benign" value="<?php echo $_SESSION['benign'];?>">
<div class="x_content">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<?php if ($_SESSION['malignan']>$_SESSION['benign']) {
				echo "<h3><b>Malignant (Kanker Ganas)</b></h3>";
				$temp = $_SESSION['malignan']/($_SESSION['malignan']+$_SESSION['benign'])*100;
				
			}else{
				echo "<h3><b>Benign (Kanker Jinak)</b></h3>";
				$temp = $_SESSION['benign']/($_SESSION['malignan']+$_SESSION['benign'])*100;
				
			} ?>
			<h4>Data input :</h4> 
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p>Clump Thickness = <?php echo $_SESSION['a1']; ?></p>
				<p>Uniformity of Cell Size = <?php echo $_SESSION['a2']; ?></p>
				<p>Uniformity of Cell Shape = <?php echo $_SESSION['a3']; ?></p>
				<p>Marginal Adhesion = <?php echo $_SESSION['a4']; ?></p>
				<p>Single Epithelial Cell Size = <?php echo $_SESSION['a5']; ?></p>
				<p>Bare Nuclei = <?php echo $_SESSION['a6']; ?></p>
				<p>Bland Chromatin = <?php echo $_SESSION['a7']; ?></p>
				<p>Normal Nucleoli = <?php echo $_SESSION['a8']; ?></p>
				<p>Mitoses = <?php echo $_SESSION['a9']; ?></p>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_content">
                 <canvas id="pieChart"></canvas>
             </div>
		</div>
		<input type="hidden" id="att1" value="<?php echo $_SESSION['att1']; ?>">
		<input type="hidden" id="att2" value="<?php echo $_SESSION['att2']; ?>">
		<input type="hidden" id="att3" value="<?php echo $_SESSION['att3']; ?>">
		<input type="hidden" id="att4" value="<?php echo $_SESSION['att4']; ?>">
		<input type="hidden" id="att5" value="<?php echo $_SESSION['att5']; ?>">
		<input type="hidden" id="att6" value="<?php echo $_SESSION['att6']; ?>">
		<input type="hidden" id="att7" value="<?php echo $_SESSION['att7']; ?>">
		<input type="hidden" id="att8" value="<?php echo $_SESSION['att8']; ?>">
		<input type="hidden" id="att9" value="<?php echo $_SESSION['att9']; ?>">
		<div class="col-md-12 col-sm-12 col-xs-12 x_content">
            <center>><div id="myDiv"></div></center>
		</div>
	</div>
</div>
<?php } require 'footer.php'; ?>