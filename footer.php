</div>

    <!-- footer content -->
    <footer>
      <div class="copyright-info">
        <p class="pull-right">Breast Cancer Diagnostic - Decision Support System 2016
        </p>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->

  <script src="js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="js/chartjs/chart.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <!-- input mask -->
  <script src="js/input_mask/jquery.inputmask.js"></script>
  <!-- range slider -->
  <script src="js/ion_range/ion.rangeSlider.min.js"></script>
  <script>
      $("#zoom_01").elevateZoom();
 </script>
  <!-- input_mask -->
  <script>
    $(document).ready(function() {
      $(":input").inputmask();
    });
  </script>
  <!-- /input mask -->
  <!-- ion_range -->
  <script>
    $(function() {
      $("#range_31").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_32").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_33").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_34").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_35").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_36").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_37").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_38").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $("#range_39").ionRangeSlider({
        type: "double",
        min: 1,
        max: 10,
        from: 1,
        to: 1,
        from_fixed: true
      });
      $(".range_min_max").ionRangeSlider({
        type: "double",
        min: 0,
        max: 100,
        from: 30,
        to: 70,
        max_interval: 50
      });
      $(".range_time24").ionRangeSlider({
        min: +moment().subtract(12, "hours").format("X"),
        max: +moment().format("X"),
        from: +moment().subtract(6, "hours").format("X"),
        grid: true,
        force_edges: true,
        prettify: function(num) {
          var m = moment(num, "X");
          return m.format("Do MMMM, HH:mm");
        }
      });
    });
  </script>
  <!-- /ion_range -->

  <script>
$(document).ready(function(){ 
    // Pie chart
    var ctx = document.getElementById("pieChart");
    var data = {
      datasets: [{
        data: [$('#benign').val(),$('#malignan').val()],
        backgroundColor: [
          "#3498db",
          "#FF5678"
        ],
        label: 'My dataset' // for legend
      }],
      labels: [
        "Benign",
        "Malignant"
      ]
    };
    var pieChart = new Chart(ctx, {
      data: data,
      type: 'pie',
      otpions: {
        legend: false
      }
    });
  });
  </script>
  <script src="js/dataBoxplot.js"></script>

    <script>
    // Get the modal
    var modal = document.getElementById('myModal');
    var modal2 = document.getElementById('myModal2');
    var modal3 = document.getElementById('myModal3');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById('myImg');
    var modalImg = document.getElementById("img01");
    var modalImg2 = document.getElementById("img02");
    var modalImg3 = document.getElementById("img03");
    var modalImg4 = document.getElementById("img04");
    var captionText = document.getElementById("caption");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        modalImg.alt = this.alt;
        captionText.innerHTML = this.alt;
    }

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() { 
        modal.style.display = "none";
    }
    </script>



</body>
</html>
