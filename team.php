<?php include 'header.php'; ?>
<div class="x_title">
    <h2>Our Team</h2>
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <div class="clearfix"></div>
      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Data Analyst</i></h4>
            <div class="left col-xs-7">
              <h2>Tri Sony Saragih</h2>
              <p><strong>About: </strong>Data Analysis</p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/team2.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
              <p class="ratings">
                <a>4.0</a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star-o"></span></a>
              </p>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i> <i class="fa fa-comments-o"></i> </button>
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Isi Sendiri :D</i></h4>
            <div class="left col-xs-7">
              <h2>Irfan Kurnia Dinata</h2>
              <p><strong>About: Coba isi sendiri :D</strong></p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/team5.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
              <p class="ratings">
                <a>4.0</a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star-o"></span></a>
              </p>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i> <i class="fa fa-comments-o"></i> </button>
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Front End Programmer</i></h4>
            <div class="left col-xs-7">
              <h2>Muhammad Alvian S</h2>
              <p><strong>About: </strong> Front End / Back End / Data Analysis </p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/team1.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
              <p class="ratings">
                <a>4.0</a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star-o"></span></a>
              </p>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i> <i class="fa fa-comments-o"></i> </button>
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>
    
      <br>
    
    <div class="col-md-1"></div>
        
      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Back End Programmer</i></h4>
            <div class="left col-xs-7">
              <h2>Ryan Baskara</h2>
              <p><strong>About: </strong> Web Designer / UX / Back End Programmer</p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/team4.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
              <p class="ratings">
                <a>4.0</a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star-o"></span></a>
              </p>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i> <i class="fa fa-comments-o"></i> </button>
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>
        
      <div class="col-md-1"></div>
        
      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Isi sendiri :D</i></h4>
            <div class="left col-xs-7">
              <h2>Miftah Faqih</h2>
              <p><strong>About: </strong>Coba isi sendiri :D </p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/team3.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center">
            <div class="col-xs-12 col-sm-6 emphasis">
              <p class="ratings">
                <a>4.0</a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star"></span></a>
                <a href="#"><span class="fa fa-star-o"></span></a>
              </p>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i> <i class="fa fa-comments-o"></i> </button>
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>

</div>
<?php include 'footer.php'; ?>