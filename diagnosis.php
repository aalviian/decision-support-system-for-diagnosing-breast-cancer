<?php require 'header.php' ?>
    <div class="x_title">
      <h2>Input value of atribut between 1 to 10</h2>
      
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <form class="form-horizontal form-label-left" method="post" action="proses.php">          
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Clump Thickness: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_31" value="" name="a1"/>
            </div>
          </div>
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Uniformity of Cell Size: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_32" value="" name="a2"/>
            </div>
          </div>
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Uniformity of Cell Shape: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_33" value="" name="a3"/>
            </div>
          </div>
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Marginal Adhesion: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_34" value="" name="a4"/>
            </div>
          </div>
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Single Epithelial Cell Size: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_35" value="" name="a5"/>
            </div>
          </div> 
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Bare Nuclei: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_36" value="" name="a6"/>
            </div>
          </div> 
            
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Bland Chromatin: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_37" value="" name="a7"/>
            </div>
          </div>  
        
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Normal Nucleoli: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_38" value="" name="a8"/>
            </div>
          </div> 

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Mitoses: <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="range_39" value="" name="a9"/>
            </div>
          </div> 
<!-- 
          <div class="col-md-4">
            <p>Clump Thickness</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a1" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Uniformity of Cell Size</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a2" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Uniformity of Cell Shape</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a3" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Marginal Adhesion</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a4" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Single Epithelial Cell Size</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a5" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Bare Nuclei</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a6" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Bland Chromatin</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a7" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Normal Nucleoli</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a8" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div>
          <div class="col-md-4">
            <p>Mitoses</p>
            <input class="knob" data-width="100" data-height="120" data-min="1" data-max="10" name="a9" data-displayPrevious=true data-fgColor="#26B99A" value="1">
          </div> -->
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-4">
                <button type="submit" name="submit" class="btn btn-success">Diagnosis</button>
            </div>
            <div class="col-md-4"></div>
        </div>
      </form>
    </div>
<br>
<?php require 'footer.php' ?>