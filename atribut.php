<?php include 'header.php'; ?>
<div class="x_title">
    <h2>Attributes Description</h2>
    <div class="clearfix"></div>
</div>
<div class="x_content">
	<div class="row">
		<h4><b>Clump Thickness</b></h4> 
		<blockquote><p>sel-sel jinak yang cenderung dikelompokkan dalam monolayer/satu lapisan, sedangkan sel-sel kanker dikelompokkan dalam multilayer.</p></blockquote> 
		<h4><b>Uniformity of Cell Size</b></h4>
        <blockquote><p>sel-sel kanker yang cenderung bervariasi dalam ukuran, semakin banyak variasi ukurannya maka semakin perlu dalam menentukan apakah sel tersebut kanker atau tidak.</p></blockquote>
		<h4><b>Uniformity of Cell Shape</b></h4> 
        <blockquote><p>sel-sel kanker yang cenderung bervariasi dalam bentuknya, sehingga perlu dalam menentukan apakah sel tersebut kanker atau tidak.</p></blockquote>
		<h4><b>Marginal Adhesion</b></h4> 
        <blockquote><p>sel normal cenderung tetap bergerombol(bersama-sama). Sel kanker cenderung memisah. Jadi, kehilangan bentuk adhesi ini menandakan keganasan sel.</p></blockquote>
		<h4><b>Single Epithelial Cell size</b></h4> 
        <blockquote><p>berkaitan dengan keseragaman ukuran sel. Sel-sel epital yang signifikan membesar kemungkinan akan menjadi sel-sel ganas yang mengakibatkan kanker.</p></blockquote>
		<h4><b>Bare Nuclei</b></h4> 
        <blockquote><p>merupakan istilah yang digunakan untuk inti sel yang tidak dikelilingi oleh sitoplasma. Biasanya berada pada tumor jinak.</p></blockquote>
		<h4><b>Bland Chromathin</b></h4> 
        <blockquote><p>merupakan keseragaman tekstur dari nucleus atau inti sel yang dapat dilihat dalam sel jinak. Dalam sel-sel kanker kromatin ini cenderung kasar.</p></blockquote>
		<h4><b>Normal Nucleoli</b></h4> 
        <blockquote><p>merupakan struktur kecil yang terlihat dalam inti sel. Dalam sel normal nucleolus ini biasanya sangat kecil bahkan tidak terlihat. Dalam sel-sel kanker nucleolus ini lebih menonjol, dan kadang-kadang lebih besar dari inti sel.</p></blockquote>
		<h4><b>Mitoses</b></h4> 
        <blockquote><p>merupakan proses pembelahan sel tunggal sehingga menjadi dua sel yang identik. Keadaan normal sel melakukan mitosis sangat rendah. Dalam keadaan kanker sel akan melakukan mitosis dengan sangat tinggi.</p></blockquote>
	</div>
</div>
<?php include 'footer.php'; ?>